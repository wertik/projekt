package scenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import mainGame.MainProgram;

public class OpeningScreen { 

	public static Scene menu() {
					
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		grid.getStylesheets().add(MainProgram.class.getResource("/css/settings.css").toExternalForm());
		
		Text scenetitle = new Text("Duck Hunter 2017");
		grid.add(scenetitle, 0, 0, 3, 1);
		scenetitle.setId("Title");

		Button btn = new Button();
		btn.setText("Play");
		grid.add(btn, 0, 1);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(scenes.ChooseBackground.background());
			}
		});

		Button btn2 = new Button();
		btn2.setText("Difficulty");
		grid.add(btn2, 0, 2);

		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(scenes.Difficulty.options());
			}
		});


		Button btn3 = new Button();
		btn3.setText("Exit");
		grid.add(btn3, 0, 3);

		btn3.setOnAction(new EventHandler<ActionEvent>() { 

			@Override
			public void handle(ActionEvent e) {

				System.exit(0);

			}
		});

		Scene menu = new Scene(grid, mainGame.MainProgram.width,mainGame.MainProgram.height);
		return menu;
	}
}