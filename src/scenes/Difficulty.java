package scenes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Difficulty {
	public static int speed = 15;
	public static Scene options() {

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		grid.getStylesheets().add(Difficulty.class.getResource("/css/settings.css").toExternalForm());
		
		Text scenetitle = new Text("Choose difficulty");
		grid.add(scenetitle, 0, 0, 3, 1);
		scenetitle.setId("Title");
		
		Button btn = new Button();
		btn.setText("Easy");
		grid.add(btn, 0, 1);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				speed = 15; //raskustaseme v��rtuse muutmine
				mainGame.MainProgram.primaryStage.setScene(scenes.OpeningScreen.menu());
			}
		});

		Button btn2 = new Button();
		btn2.setText("Normal");
		grid.add(btn2, 0, 2);
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				speed = 10; //raskustaseme v��rtuse muutmine
				mainGame.MainProgram.primaryStage.setScene(scenes.OpeningScreen.menu());
			}
		});
		Button btn3 = new Button();
		btn3.setText("Hard");
		grid.add(btn3, 0, 3);
		btn3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				speed = 5; //raskustaseme v��rtuse muutmine
				mainGame.MainProgram.primaryStage.setScene(scenes.OpeningScreen.menu());
			}
		});

		Scene options = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		return options;
	}
}
