package scenes;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import mainGame.ValueName;

public class ChooseBackground {

	public static Scene background() {
		
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		grid.getStylesheets().add(Difficulty.class.getResource("/css/settings.css").toExternalForm());
		
		Text scenetitle = new Text("Choose background");
		grid.add(scenetitle, 2, 0, 1, 1);
		scenetitle.setId("Title");

		ImageView option1 = new ImageView("images/background2.jpg");
		option1.setFitHeight(mainGame.MainProgram.height * 0.2);
		option1.setFitWidth(mainGame.MainProgram.width * 0.2);
		option1.setPreserveRatio(true);
		Button btn = new Button("", option1);
		grid.add(btn, 2, 1);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				
				ValueName.name = "/css/settings2.css";
				mainGame.MainProgram.primaryStage.setScene(scenes.GameBackground.game());
				
							
			}
		});

		ImageView option2 = new ImageView("images/background3.jpg");
		option2.setFitHeight(mainGame.MainProgram.height * 0.2);
		option2.setFitWidth(mainGame.MainProgram.width * 0.2);
		option2.setPreserveRatio(true);
		Button btn2 = new Button("", option2);
		grid.add(btn2, 2, 2);
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				
				ValueName.name = "/css/settings3.css";
				mainGame.MainProgram.primaryStage.setScene(scenes.GameBackground.game());
			}
		});

		Scene background = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		return background;
	}
}