package scenes;

import javafx.animation.AnimationTimer;
import javafx.animation.PathTransition;
import javafx.event.EventHandler;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import mainGame.Circle;
import javafx.scene.shape.Polyline;
import javafx.util.Duration;
import mainGame.ValueName;
import mainGame.MainProgram;

public class GameBackground {
 public static ValueName time = new ValueName(0); 
 int begintime = (int) time.value;
 
	public static Scene game() {
		
		double heigth = mainGame.MainProgram.height;
		double width = mainGame.MainProgram.width;

		ValueName x = new ValueName(0);
		ValueName y = new ValueName(0);

		ValueName points = new ValueName(0);
		Pane root = new Pane();
		root.getStylesheets().add(MainProgram.class.getResource(mainGame.ValueName.name).toExternalForm());

		Image image = new Image("/images/crosshair.png");
		root.setCursor(new ImageCursor(image));

		Circle targetData = new Circle(x.value, y.value, 30);
		javafx.scene.shape.Circle target = new javafx.scene.shape.Circle(x.value, y.value, 30);

		ImageView duck = new ImageView(new Image("/images/duck1.png"));
		duck.setFitHeight(100);
		duck.setFitWidth(100);

		Image duck1 = new Image("/images/duck1.png");
		Image duck2 = new Image("/images/duck2.png");

		new AnimationTimer() {
			public void handle(long currentNanoTime) {
				time.value = (int) (currentNanoTime / 100000000);
				x.value = (int) duck.getTranslateX() + 40;
				y.value = (int) duck.getTranslateY() + 40;
				targetData.setCenter(x.value, y.value);
				duck.setPreserveRatio(true);
				target.setCenterX(x.value);
				target.setCenterY(y.value);
				if (time.value % 5 == 0) {

					duck.setImage(duck1);
				}
				if (time.value % 10 == 0) {
					duck.setImage(duck2);

				}
				if (points.value == 1) {
					mainGame.MainProgram.primaryStage.setScene(scenes.Endscreen.end());
					stop();
					return;
				}

			}
		}.start();

		root.getChildren().add(duck);
		Polyline polyline = new Polyline();

		polyline.getPoints().addAll(new Double[] { -20.0, Math.random() * heigth, width / 3, heigth / 3 * Math.random(),
				width / 3 * 2, heigth / 3 * Math.random(), width + 30.0, Math.random() * heigth }); // pardi lennu koordinaadid
		PathTransition lend = new PathTransition();
		lend.setNode(duck);
		lend.setDuration(Duration.seconds(scenes.Difficulty.speed)); // raskustase
		lend.setPath(polyline);
		lend.setCycleCount(PathTransition.INDEFINITE);
		lend.play();

		root.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				if (targetData.containsPoint(e.getX(), e.getY())) {
					points.value++;

					System.out.println("Bullseye! Points:" + points.value);
					root.getChildren().remove(duck);
					root.getChildren().remove(target);

				}
			}
		});

		Scene game = new Scene(root, mainGame.MainProgram.width, mainGame.MainProgram.height);
		return game;
	}

}