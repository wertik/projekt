package scenes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Endscreen {

	public static Scene end() {

		String result;
		if (scenes.GameBackground.time.value < 200000) {
			result = " well";
			
			
		} else {
			result = " bad";
			
		}

		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(30);
		grid.setVgap(30);
		grid.setPadding(new Insets(25, 25, 25, 25));
		grid.getStylesheets().add(Endscreen.class.getResource("/css/settings1.css").toExternalForm());

		Text scenetitle = new Text("Game over! You did" + result);
		grid.add(scenetitle, 0, 0, 1, 1);
		scenetitle.setId("Title");

		Button btn = new Button();
		btn.setText("Back to main menu");
		grid.add(btn, 0, 1);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				mainGame.MainProgram.primaryStage.setScene(scenes.OpeningScreen.menu());
			}
		});

		Scene end = new Scene(grid, mainGame.MainProgram.width, mainGame.MainProgram.height);
		return end;
	}
}
