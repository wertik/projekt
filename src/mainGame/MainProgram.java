package mainGame;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Java l�puprojekt. T�nap�evane lihtsustatud versioon 1984. aasta tuntud m�ngust Duck Hunter.
 * Algul on v�imalik valida men��st 3 erineva raskustaseme vahelt, mis muudavad pardi lendamise kiirust. Vaikimisi on raskustase easy ehk lihtne.
 * Samuti on v�imalik enne m�ngu alustamist valida endale meeldiv m�ngutaust. M�ng l�peb kui pardile on pihta saadud.
 * @author Erik Kaup
 */

public class MainProgram extends Application {

	public static void main(String[] args) {
		launch(args);

	}

	public static Stage primaryStage;

	@Override
	public void start(Stage Stage) {
		primaryStage = Stage;
		Stage.setWidth(width);
		Stage.setHeight(height);
		primaryStage.setTitle("Duck Hunter 2017");
		primaryStage.setScene(scenes.OpeningScreen.menu());
		primaryStage.show();

	}

	static Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
	public static int width = (int) primaryScreenBounds.getWidth();
	public static int height = (int) primaryScreenBounds.getHeight();
}
